import configparser
import json

config = configparser.ConfigParser()
config.read("connection.properties")
check_sections = config.sections()

def get_url():
    url_section = config["connection_url"]
    base_url = "".join(url_section[j] for j in url_section)
    return base_url

def get_watchlist_status_param():
    params = config["get_watchlist_param"]
    watchlist_version = config["get_watchlist_param"]["additional_param1"]
    # print(watchlist_version)
    version = watchlist_version.split("-")[1]
    get_param = "/".join(params[i] for i in params)
    return get_param

def get_token():
    current_token = config["aws_token"]
    token = "".join(current_token[k] for k in current_token)
    return token
