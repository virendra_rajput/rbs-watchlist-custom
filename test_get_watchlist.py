import requests
import json
from get_config import *

result = {}
base_url = get_url()
token = get_token()

def check_lists(json_val):
    if(len(json_val) == 1) :
        result["error"] = True
        result["message"] = "No proper URL"
    elif (len(json_val) == 2):
        if "watchlist" in json_val:
            check_obj = json_val["watchlist"][0]
            if (len(check_obj) == 6) :
                result["success"] = True
    print(result)





def test_check_watchlists():
    # url = "https://2qd3sqzktg.execute-api.eu-west-1.amazonaws.com/Prod/v1/watchlists"
    url = base_url + "watchlists"
    headers = {
        'x-api-key': token
    }
    response = requests.request("GET", url, headers=headers)
    # print(response.text)

    wl_json = json.loads(response.text)
    check_lists(wl_json)

    # try:
    #     check_lists(wl_json)
    # except KeyError:
    #     print("Some Keys are missing")
    # except :
    #     print("something else wrong here")