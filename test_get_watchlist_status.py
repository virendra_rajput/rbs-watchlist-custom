import requests
import json
import configparser

result ={}
config = configparser.ConfigParser()
# a = config.sections()         #no need of this one
config.read("connection.properties")
each_section = config.sections()

# get param for current file
params = config["get_watchlist_param"]
watchlist_version = config["get_watchlist_param"]["additional_param1"]
print(watchlist_version)
version = watchlist_version.split("-")[1]
get_param = "/".join(params[i] for i in params)

#get current URL
url_section = config["connection_url"]
base_url = "".join(url_section[j] for j in url_section)
# print(base_url)

#Get Token
current_token = config["aws_token"]
token = "".join(current_token[k] for k in current_token)



def check_validate(value):
    if (value["status"] == "done"):
        result["status"] = "fine"
        if (value["records_count"] > 0):
            result["records_count"] = "fine"
        else:
            result["records_count"] = "check the file"
    else:
        result["status"] = "pending"
    
    if (value["version"] == version):
        result["version"] = "fine"
    else:
        result["version"] = "not matching"
    
    print(result)

def test_check_watchlist():
    # url = "https://2qd3sqzktg.execute-api.eu-west-1.amazonaws.com/Prod/v1/watchlist/wcp-20190704123-person-1.7.23"
    url = str(base_url) + str(get_param) 
    # print(url)
    resp_key = url.split("/")
    resp_key = resp_key[6]
    headers = {
    'x-api-key':token,
    'User-Agent': "PostmanRuntime/7.19.0",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "30d20983-9439-40ae-805c-a08da08c4a7e,490bf70e-2682-47d7-96cf-7bf36e5d6fa6",
    'Host': "2qd3sqzktg.execute-api.eu-west-1.amazonaws.com",
    'Accept-Encoding': "gzip, deflate",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }
    print("requesting...")
    response = requests.request("GET", url, headers=headers)
    
    value = json.loads(response.text)
    
    try :
        check_validate(value[watchlist_version])
    except KeyError:
        print("There's no such Watchlist available")
    except :
        print("whats going on!")



